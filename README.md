# Git history

Browse/view the git log

## Usage

`git-history -n <recent-commits>`

## Planned
- support paged scrolling through whole history
- support graceful quitting (right now Ctrl-C is the only way)
- fix intermittent crashes on weird input when in commit select menu
