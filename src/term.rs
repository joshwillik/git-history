use std::io;
use std::io::Write;

pub fn clear() {
    print!("{}[2J", 27 as char);
    io::stdout().flush().unwrap();
}
