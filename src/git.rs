use std::process::Command;
use std::fmt;
const LINE_SEPARATOR: &'static str = "~_-_-_~";
const FIELD_SEPARATOR: &'static str = "!*!@!*!";

pub fn commits(n_recent: usize) -> Vec<Commit> {
    let field_format = vec!["%h", "%an", "%ae", "%ai", "%s", "%b", "%N"]
    .join(FIELD_SEPARATOR);
    let cmd = Command::new("git")
        .arg("log")
        .arg(format!("--format={}{}", field_format, LINE_SEPARATOR))
        .arg(format!("HEAD~{}..HEAD", n_recent))
        .output()
        .unwrap();
    return String::from_utf8(cmd.stdout)
        .unwrap()
        .split(LINE_SEPARATOR)
        .map(|s| s.trim())
        .filter(|s| s.len()>0)
        .map(parse_commit)
        .collect();
}

pub fn show(commit_id: String) {
    Command::new("git")
        .arg("-c")
        .arg("diff.tool=vimdiff")
        .arg("-c")
        .arg("difftool.trustExitCode=true")
        .arg("difftool")
        .arg("--no-prompt")
        .arg(format!("{}^..{}", commit_id, commit_id))
        .spawn()
        .unwrap()
        .wait()
        .unwrap();
}

fn parse_commit(log_line: &str) -> Commit {
    let fields: Vec<&str> = log_line.split(FIELD_SEPARATOR).collect();
    let mut fields = fields.iter();
    let mut next = || fields.next().unwrap().to_string();
    Commit{
        id: next(),
        name: next(),
        email: next(),
        date: next(),
        subject: next(),
        body: next(),
        notes: next(),
    }
}

pub struct Commit {
    pub id: String,
    pub name: String,
    pub email: String,
    pub date: String,
    pub subject: String,
    pub body: String,
    pub notes: String,
}

impl fmt::Display for Commit {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} <{}> [{}] {}", self.subject, self.email, self.date, self.id)
    }
}
