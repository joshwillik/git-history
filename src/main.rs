use std::process;
use dialoguer::Select;
use std::env;
mod git;
mod term;

fn main() {
    let args = parse_args();
    let commits = git::commits(args.n_commits);
    let mut selected = 0;
    loop {
        match select_commit(&commits, &selected) {
            Some(idx) => selected = idx,
            None => return,
        };
        git::show(commits[selected].id.clone());
    }
}

fn parse_args() -> Args {
    let mut args = Args {
        n_commits: 0,
    };
    let mut flag = String::new();
    for arg in env::args().skip(1) {
        println!("arg: {:?}", arg);
        if arg == "-n" {
            flag = arg.to_string();
            continue;
        }
        if flag == "-n" {
            if let Ok(n) = arg.parse() {
                args.n_commits = n;
                continue;
            }
        }
        usage();
    }
    if args.n_commits==0 {
        usage();
    }
    args
}

struct Args {
    n_commits: usize,
}

fn usage() {
    eprintln!("Usage: git-history -n <recent-commits>");
    process::exit(1);
}

fn select_commit(commits: &Vec<git::Commit>, selected: &usize) -> Option<usize> {
    term::clear();
    let selected = Select::new()
        .items(commits)
        .default(*selected)
        .interact()
        .unwrap();
    return Some(selected);
}
